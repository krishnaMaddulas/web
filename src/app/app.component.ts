import { Component } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'SampleAngular';

  constructor(private router: Router) { }

  redirectToSignUpPage() {
    console.log("Redirecting to sign up page");
    // this.router.navigate(['../signup']);
  }

  redirectToLoginPage() {
    console.log("Redirect to login page");
  }
}
